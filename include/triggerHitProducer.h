#ifndef TRIGGERRHITPRODUCER_H
#define TRIGGERRHITPRODUCER_H

#include <iostream>
#include <vector>
#include "triggerHit.h"
#include "skiroc2cms.h"
#include "pedestalData.h"

class triggerHitProducer
{

 public:
  triggerHitProducer(){;}
  triggerHitProducer(std::string pedestalFileName);
  void produceTriggerHits( std::vector<skiroc2cms>& skirocs, std::vector<triggerHit>& hits );

 private:
  pedestalData m_pedData;
  std::vector<float> m_adcHigh,m_adcLow;
};

#endif
