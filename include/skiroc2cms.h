#ifndef SKIROC2CMS
#define SKIROC2CMS

#include <iostream>
#include <vector>
#include <bitset>
#include <iomanip>
#include <cstring>

static const unsigned int  MASK_ADC = 0x0FFF;
static const unsigned int  MASK_ROLL = 0x1FFF;
static const unsigned int  MASK_GTS_MSB = 0x3FFF;
static const unsigned int  MASK_GTS_LSB = 0x1FFF;
static const unsigned int  MASK_ID = 0xFF;
static const unsigned int  MASK_HEAD = 0xF000;

static const unsigned int  N_CHANNELS_PER_SKIROC = 64;
static const unsigned int  NUMBER_OF_SCA = 13;
static const unsigned int  ADCLOW_SHIFT = 0;
static const unsigned int  ADCHIGH_SHIFT = 64;
static const unsigned int  SCA_SHIFT = 128;
static const unsigned int  SKIROC_DATA_SIZE = 1924; //number of 16 bits words

class skiroc2cms
{
public:
  skiroc2cms(){;}
  skiroc2cms( const uint16_t* data );

  std::vector<int> rollPositions() const;
  uint16_t gray_to_binary(const uint16_t gray) const;
  bool check(bool printErrors=false);

  uint16_t ADCLow( int chan, int sca ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; sca=NUMBER_OF_SCA-1-sca; return (sca>=0 && sca<NUMBER_OF_SCA) ? gray_to_binary( m_data[chan+ADCLOW_SHIFT+SCA_SHIFT*sca] & MASK_ADC ) : 10000;}
  uint16_t ADCHigh( int chan, int sca ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; sca=NUMBER_OF_SCA-1-sca; return (sca>=0 && sca<NUMBER_OF_SCA) ? gray_to_binary( m_data[chan+ADCHIGH_SHIFT+SCA_SHIFT*sca] & MASK_ADC ) : 10000;}
  uint16_t TOTFast( int chan ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return gray_to_binary( m_data[chan+ADCLOW_SHIFT+SCA_SHIFT*(NUMBER_OF_SCA+1)] & MASK_ADC );}
  uint16_t TOTSlow( int chan ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return gray_to_binary( m_data[chan+ADCHIGH_SHIFT+SCA_SHIFT*(NUMBER_OF_SCA+1)] & MASK_ADC );}
  uint16_t TOAFall( int chan ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return gray_to_binary( m_data[chan+ADCLOW_SHIFT+SCA_SHIFT*NUMBER_OF_SCA] & MASK_ADC );}
  uint16_t TOARise( int chan ) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return gray_to_binary( m_data[chan+ADCHIGH_SHIFT+SCA_SHIFT*NUMBER_OF_SCA] & MASK_ADC );}
  bool TOAHitFall(int chan) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return ((m_data[chan+ADCLOW_SHIFT]&~MASK_ADC)>>4*3)&0x1 ;}
  bool TOAHitRise(int chan) const {chan=N_CHANNELS_PER_SKIROC-1-chan; return ((m_data[chan+ADCHIGH_SHIFT]&~MASK_ADC)>>4*3)&0x1 ;}
  uint16_t rollMask() const { return (m_data[SKIROC_DATA_SIZE-4]&MASK_ROLL); }

private:
  uint16_t* m_data;

public:
  
  friend std::ostream& operator<<(std::ostream& s, const skiroc2cms& ski)
  {
    std::vector<int> rollpositions=ski.rollPositions();
    std::cout << "rollMask = " << ski.rollMask() << "\t";
    for(size_t i=0; i<13; i++)
      std::cout << std::dec << rollpositions[i] << " ";
    std::cout << "\n";
    for (size_t i = 0; i < N_CHANNELS_PER_SKIROC; i++){
      s << "\n Channel det id : " << std::dec << i << "\n High gain ADC => " << ski.TOAHitRise(i) ;
      for( size_t j=0; j<NUMBER_OF_SCA; j++)
	s << " " << std::dec << ski.ADCHigh(i,j) ;
      s << " " << std::dec << ski.TOARise(i) ;
      s << " " << std::dec << ski.TOTFast(i) ;
      s << "\n Low gain ADC => " << ski.TOAHitFall(i) ;
      for( size_t j=0; j<NUMBER_OF_SCA; j++)
	s << " " << std::dec << ski.ADCLow(i,j) ;
      s << " " << std::dec << ski.TOAFall(i) ;
      s << " " << std::dec << ski.TOTSlow(i) ;
    }
    s << std::endl;
    return s;
  }

};

#endif
