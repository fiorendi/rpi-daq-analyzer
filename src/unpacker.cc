#include "unpacker.h"
#include "skiroc2cms.h"

#include <cstring>

unpacker::unpacker(std::string fileName, unsigned int headerSize,unsigned int eventTrailerSize, unsigned int nSkipEvents, unsigned int maxEvents, bool compressedData, std::string hexaboardType)
{
  m_fileName         = fileName;
  m_headerSize       = headerSize;
  m_eventTrailerSize = eventTrailerSize;
  m_nSkipEvents      = nSkipEvents;
  m_maxEvents        = maxEvents;
  m_compressedData   = compressedData;

  char *m_header=new char[m_headerSize];
  m_inputstream.open(m_fileName.c_str(), std::ios::in|std::ios::binary);
  if( !m_inputstream.is_open() ){
    std::cout << "PROBLEM : Not able to open " << m_fileName << "\t -> return false (so end of process I guess)" << std::endl;
    return;
  }
  m_inputstream.seekg( 0, std::ios::beg );
  m_inputstream.read( m_header, m_headerSize );
  
  std::cout << "bit string = \t";
  for( unsigned int i=0; i<m_headerSize/2; i++)
    std::cout << " " 
	      << std::setw(2) << std::setfill('0') << std::hex << int(m_header[2*i+1]&0xff) 
	      << std::setw(2) << std::setfill('0') << std::hex << int(m_header[2*i]&0xff) ;
  std::cout << std::endl;

  // std::vector<uint16_t> bitstring;
  // for( int i=0; i<int(m_headerSize/2); i++ ){
  //   uint16_t aint=0;
  //   char buf[] = {m_header[i*2],m_header[i*2+1]};
  //   memcpy(&aint, &buf, sizeof(aint));
  //   bitstring.push_back(aint);
  // }
  // std::cout << "bit string = \t";
  // for( std::vector<uint16_t>::iterator it=bitstring.begin(); it!= bitstring.end(); ++it )
  //   std::cout << " " << std::setfill('0') << std::setw(4) << std::hex << (*it) ;
  // std::cout << std::endl;

  m_event=0;
  m_nWords=30784;
  if( m_compressedData ) m_nWords=15392;
  m_buffer=new char[m_nWords+m_eventTrailerSize];

  if( hexaboardType=="8inch" ){
    m_sktoIC[2]=3;
    m_sktoIC[3]=2;
  }

}
 
bool unpacker::unpack( std::vector<skiroc2cms>& skirocs )
{
  uint16_t decodedData[4][SKIROC_DATA_SIZE];

  uint64_t nBytesToSkip=m_headerSize+m_nSkipEvents*(m_nWords+m_eventTrailerSize);
  m_inputstream.seekg( (std::streamoff)nBytesToSkip+(std::streamoff)m_event*(m_nWords+m_eventTrailerSize) );
  m_inputstream.read ( m_buffer, m_nWords+m_eventTrailerSize );
  if( !m_inputstream.good() ){
    m_inputstream.close();
    return false;
  }
  uint16_t evtTrailer;
  char buf[] = {m_buffer[m_nWords],m_buffer[m_nWords+1]};
  memcpy(&evtTrailer, &buf, sizeof(evtTrailer));
  //std::cout << "evtTrailer = " << std::setfill('0') << std::setw(4) << std::hex << evtTrailer << std::endl;

  //decode the raw data:
  uint32_t x,y;
  for (int sk = 0; sk < 4; sk++){
    for (unsigned int row = 0; row < SKIROC_DATA_SIZE; row++)
      decodedData[sk][row]=0;
  }

  if( !m_compressedData ){
    for(unsigned int  i = 0; i < SKIROC_DATA_SIZE; i++){
      for (int j = 0; j < 16; j++){
	x = m_buffer[i*16 + j]&0xff;
	x = x&0xf;
	for (int sk = 0; sk < 4; sk++)
	  decodedData[sk][i] = decodedData[sk][i] | (uint16_t) (((x >> m_sktoIC[sk] ) & 1) << (15 - j));
      }
    }
  }
  else{
    for(unsigned int  i = 0; i < SKIROC_DATA_SIZE; i++){
      for (int j = 0; j < 8; j++){
	x = m_buffer[i*8 + j]&0xff;
	y = (x>>4)&0xf;
	x = x&0xf;
	for (int sk = 0; sk < 4; sk++){
	  decodedData[sk][i] = decodedData[sk][i] | (uint16_t) (((x >> m_sktoIC[sk] ) & 1) << (14 - j*2));
	  decodedData[sk][i] = decodedData[sk][i] | (uint16_t) (((y >> m_sktoIC[sk] ) & 1) << (15 - j*2));
	}
      }
    }
  }

  for(int sk=0; sk<4; sk++){
    skiroc2cms skiroc(decodedData[sk]);
    skirocs.push_back(skiroc);
  }
  m_event++;
  return true;
}
