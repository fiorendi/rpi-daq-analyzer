#include "triggerHit.h"

triggerHit::triggerHit( uint16_t chip, uint16_t channel, std::vector<float> &adcHigh, std::vector<float> &adcLow, float toaRise, float toaFall, float totSlow, float totFast ) 
{  
  m_chip=chip;
  m_channel=channel;
  m_adcHigh=adcHigh;
  m_adcLow=adcLow;
  m_toaRise=toaRise;
  m_toaFall=toaFall;
  m_totSlow=totSlow;
  m_totFast=totFast;
  //  m_noiseFlag=false;
}
