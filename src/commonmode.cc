#include "commonmode.h"

#include <cstring>
#include <algorithm>
#include <functional>

commonmode::commonmode( std::string mapfile, int expectedMaxTS, int threshold )
{
  FILE* f = fopen(mapfile.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", mapfile.c_str() );
    exit(1);
  }
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) *p_comment = 0;
    int ptr;
    int padID, chipID, chanID;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %n ", &padID, &chipID, &chanID, &ptr);
    if (found == 3) {
      process += ptr;
    } else continue;
    m_connectedChannelKeys.push_back( 100*chipID+chanID );
  }

  m_threshold = threshold;
  m_expectedMaxTS = expectedMaxTS > 0 ? expectedMaxTS : expectedMaxTS+1 ;

  for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
    m_adcHigh[it].reserve(NUMBER_OF_CONNECTED_CHANNELS);
    m_adcLow[it].reserve(NUMBER_OF_CONNECTED_CHANNELS);
  }
}

triggerHit commonmode::subtractCM(triggerHit hit,std::vector<float> cmhigh, std::vector<float> cmlow)
{
  triggerHit cmhit = hit;
  for(int it=0;it<11;it++){
    cmhit.setHighGainADC(it, hit.highGainADC(it) - cmhigh.at(it));
    cmhit.setLowGainADC(it, hit.lowGainADC(it) - cmlow.at(it));
  }
  return cmhit;
}

void commonmode::evalAndSubtractCommonMode( std::vector<triggerHit>& hits )
{
  for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
    m_adcHigh[it].clear();
    m_adcLow[it].clear();
  }
  for ( auto hit : hits ) {
    if( std::find( m_connectedChannelKeys.begin(), m_connectedChannelKeys.end(), hit.chip()*100+hit.channel() ) != m_connectedChannelKeys.end() ){
      float adcsum = hit.highGainADC(m_expectedMaxTS-1) + hit.highGainADC(m_expectedMaxTS) + hit.highGainADC(m_expectedMaxTS+1) - 0.6*hit.highGainADC(m_expectedMaxTS+3) ;
      if(adcsum>m_threshold) continue;
      for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
	m_adcHigh[it].push_back( hit.highGainADC(it) );
	m_adcLow[it].push_back( hit.lowGainADC(it) );
      }
    }
  }
  
  std::vector<float> cmhigh,cmlow;
  for( unsigned int it=0; it<NUMBER_OF_TIME_SAMPLES; it++ ){
    std::sort( m_adcHigh[it].begin(), m_adcHigh[it].end() );
    std::sort( m_adcLow[it].begin(), m_adcLow[it].end() );
    unsigned int vsize = m_adcLow[it].size();
    cmhigh.push_back( vsize%2==0 ? 0.5*(m_adcHigh[it][vsize/2]+m_adcHigh[it][vsize/2+1]) : m_adcHigh[it][vsize/2] );
    cmlow.push_back( vsize%2==0 ? 0.5*(m_adcLow[it][vsize/2]+m_adcLow[it][vsize/2+1]) : m_adcLow[it][vsize/2] );
  }

  std::transform( hits.begin(), hits.end(), hits.begin(),
		  std::bind(commonmode::subtractCM, std::placeholders::_1, cmhigh, cmlow )
		  );


}
